DECLARE @vIdNota int = 0
DECLARE @vNumeroNotaFiscal int = 1
DECLARE @vSerie int = 2
DECLARE @vNomeCliente varchar(50) = 'TESTE'
DECLARE @vEstadoDestino varchar(50) = 'SP'
DECLARE @vEstadoOrigem varchar(50) = 'RJ'
DECLARE @vCount INT = 0

DECLARE	@vIdItem int   
DECLARE @vCfop varchar(5)
DECLARE @vTipoIcms varchar(20)
DECLARE @vBaseIcms decimal(18,5)
DECLARE @vAliquotaIcms decimal(18,5)
DECLARE @vValorIcms decimal(18,5)
DECLARE @vNomeProduto varchar(50)
DECLARE @vCodigoProduto varchar(20)

WHILE (@vCount <= 1000) 
BEGIN
	SET @vIdNota = 0
	SET @vNumeroNotaFiscal  = @vCount + 1
	SET @vSerie = 1
	SET @vNomeCliente = 'TESTE ' + CONVERT(VARCHAR, @vCount)

	IF (@vCount % 2) = 0
	BEGIN		
		SET @vEstadoOrigem = 'RJ'
		SET @vEstadoDestino = 'SP'
	END
	ELSE
	BEGIN		
		SET @vEstadoOrigem = 'MG'
		SET @vEstadoDestino = 'PE'
	END

	DECLARE @vDataEmissao DATETIME
	SET @vDataEmissao = GETDATE()

	EXEC [dbo].[P_INCLUIR_ATUALIZAR_NOTA_FISCAL] 
		@pId = @vIdNota OUTPUT,
		@pNumeroNotaFiscal = @vNumeroNotaFiscal,
		@pDataEmissao = @vDataEmissao,
		@pSerie = @vSerie,
		@pNomeCliente = @vNomeCliente,
		@pEstadoDestino = @vEstadoDestino,
		@pEstadoOrigem = @vEstadoOrigem
		
	SET @vIdItem = 0   

	IF (@vCount % 2) = 0
		SET @vCfop = '6.102'
	ELSE
		SET @vCfop = '5.100'
	
	SET @vTipoIcms = '60'
	SET @vBaseIcms = 100.00
	SET @vAliquotaIcms = 10
	SET @vValorIcms = 10
	SET @vNomeProduto = 'PRODUTO DE CARGA'
	SET @vCodigoProduto = '123-5548-555-22'

	EXEC [dbo].[P_NOTA_FISCAL_ITEM]
		@pId = @vIdItem,
		@pIdNotaFiscal = @vIdNota,
		@pCfop = @vCfop,
		@pTipoIcms = @vTipoIcms,
		@pBaseIcms = @vBaseIcms,
		@pAliquotaIcms = @vAliquotaIcms,
		@pValorIcms = @vValorIcms,
		@pNomeProduto = @vNomeProduto,
		@pCodigoProduto = @vCodigoProduto

	SET @vCount = @vCount + 1
END

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'S�o paulo', 
	@pSigla = 'SP',
	@pCFOP = '6.000',
	@pCentroDistribuicao = 1, 
	@pEntregaDisponivel = 0

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Rio de janeiro', 
	@pSigla = 'RJ',
	@pCFOP = '6.000',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Minas gerais', 
	@pSigla = 'MG',
	@pCFOP = '6.002',
	@pCentroDistribuicao = 1,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Pernambuco', 
	@pSigla = 'PE',
	@pCFOP = '6.001',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Para�ba', 
	@pSigla = 'PB',
	@pCFOP = '6.003',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Paran�', 
	@pSigla = 'PR',
	@pCFOP = '6.004',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Piaui', 
	@pSigla = 'PI',
	@pCFOP = '6.005',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Roraima', 
	@pSigla = 'RO',
	@pCFOP = '6.006',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Sergipe',
	@pCFOP = '6.007',
	@pSigla = 'SE',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1


EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Par�', 
	@pSigla = 'PA',
	@pCFOP = '6.010',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

EXEC P_INCLUIR_ATUALIZAR_ESTADO
	@pId = 0,
	@pNomeEstado = 'Santa Catarina', 
	@pSigla = 'SC',
	@pCFOP = '6.009',
	@pCentroDistribuicao = 0,
	@pEntregaDisponivel = 1

INSERT INTO dbo.AcessosConfig (Usuario, Senha) Values ('Admin', '8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92')
