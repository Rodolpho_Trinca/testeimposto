USE [Teste]
GO

/****** Object:  Table [dbo].[NotaFiscal]    Script Date: 24/07/2015 11:58:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
if OBJECT_ID('dbo.NotaFiscal', 'U') IS NOT NULL DROP TABLE [dbo].[NotaFiscal]

CREATE TABLE [dbo].[NotaFiscal](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumeroNotaFiscal] [int] NULL,
	[DataEmissao] DATETIME NULL,
	[Serie] [int] NULL,
	[NomeCliente] [varchar](50) NULL,
	[EstadoDestino] [varchar](50) NULL,
	[EstadoOrigem] [varchar](50) NULL,
 CONSTRAINT [PK_NotaFiscal] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

if OBJECT_ID('dbo.NotaFiscalItem', 'U') IS NOT NULL DROP TABLE [dbo].[NotaFiscalItem]

CREATE TABLE [dbo].[NotaFiscalItem](
	[Id] [int]  IDENTITY(1,1) NOT NULL,
	[IdNotaFiscal] [int] NULL,
	[Cfop] [varchar](5) NULL,
	[TipoIcms] [varchar](20) NULL,
	[BaseIcms] [decimal](18, 5) NULL,
	[AliquotaIcms] [decimal](18, 5) NULL,
	[ValorIcms] [decimal](18, 5) NULL,
	[BaseIpi] [decimal](18, 5) NULL,
	[AliquotaIpi] [decimal](18, 5) NULL,
	[ValorIpi] [decimal](18, 5) NULL,
	[NomeProduto] [varchar](50) NULL,
	[CodigoProduto] [varchar](20) NULL,
	[ValorProduto] [decimal](18,5) NULL,
	[ValorDesconto] [decimal](18,5) NULL,
	[EstadoOrigem] [char](2) NULL,
	[EstadoDestino] [char](2) NULL,
	[Brinde] [bit] NULL,
 CONSTRAINT [PK_NotaFiscalItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[NotaFiscalItem] WITH CHECK ADD CONSTRAINT [FK_NotaFiscal] FOREIGN KEY([Id])
REFERENCES [dbo].[NotaFiscalItem]([Id])
GO

ALTER TABLE [dbo].[NotaFiscalItem] CHECK CONSTRAINT [FK_NotaFiscal]
GO

if OBJECT_ID('dbo.AcessosConfig', 'U') IS NOT NULL DROP TABLE [dbo].[AcessosConfig]

CREATE TABLE [dbo].[AcessosConfig](
	[Id] [int]  IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](50) NOT NULL,
	[Senha] [varchar](256) NOT NULL,
 CONSTRAINT [PK_AcessosConfig] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

if OBJECT_ID('dbo.SystemConfig', 'U') IS NOT NULL DROP TABLE [dbo].[SystemConfig]

CREATE TABLE [dbo].[SystemConfig](
	[Id] [int]  IDENTITY(1,1) NOT NULL,
	[Campo] [varchar](50) NOT NULL,
	[Valor] [varchar](100) NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

if OBJECT_ID('dbo.Estados', 'U') IS NOT NULL DROP TABLE [dbo].[Estados]

CREATE TABLE [dbo].[Estados](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NomeEstado] [varchar](50) NOT NULL,
	[CFOP] [CHAR](5) NOT NULL,
	[Sigla] [CHAR](2) NOT NULL,
	[CentroDistribuicao] [bit] DEFAULT 0,
	[EntregaDisponivel] [bit] DEFAULT 1
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
