USE [Teste]
GO
IF OBJECT_ID('dbo.P_INCLUIR_ATUALIZAR_ESTADO') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.P_INCLUIR_ATUALIZAR_ESTADO
    IF OBJECT_ID('dbo.P_INCLUIR_ATUALIZAR_ESTADO') IS NOT NULL
        PRINT '<<< FALHA APAGANDO A PROCEDURE dbo.P_INCLUIR_ATUALIZAR_ESTADO >>>'
    ELSE
        PRINT '<<< PROCEDURE dbo.P_INCLUIR_ATUALIZAR_ESTADO APAGADA >>>'
END
go
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON 
GO 
CREATE PROCEDURE dbo.P_INCLUIR_ATUALIZAR_ESTADO 
(
	@pId int OUTPUT,
	@pNomeEstado VARCHAR(50),
	@pSigla CHAR(2),
	@pCFOP CHAR(5),
	@pCentroDistribuicao bit,
	@pEntregaDisponivel bit
)
AS
BEGIN
	IF (@pId = 0)
		BEGIN 
			INSERT INTO [dbo].[Estados]
			   ([NomeEstado]
			   ,[Sigla]
			   ,[CFOP]
			   ,[CentroDistribuicao]
			   ,[EntregaDisponivel])
			VALUES
			   (@pNomeEstado
			   ,@pSigla
			   ,@pCFOP
			   ,@pCentroDistribuicao
			   ,@pEntregaDisponivel)

			SET @pId = @@IDENTITY
		END
	ELSE
		BEGIN
			UPDATE [dbo].[Estados]
			SET [NomeEstado]				= @pNomeEstado
				,[Sigla]					= @pSigla
				,[CFOP]						= @pCFOP
				,[CentroDistribuicao]		= @pCentroDistribuicao
				,[EntregaDisponivel]		= @pEntregaDisponivel
			WHERE Id = @pId
		END	    
END
GO
GRANT EXECUTE ON dbo.P_INCLUIR_ATUALIZAR_ESTADO TO [public]
go
IF OBJECT_ID('dbo.P_INCLUIR_ATUALIZAR_ESTADO') IS NOT NULL
    PRINT '<<< PROCEDURE dbo.P_INCLUIR_ATUALIZAR_ESTADO CRIADA >>>'
ELSE
    PRINT '<<< FALHA NA CRIACAO DA PROCEDURE dbo.P_INCLUIR_ATUALIZAR_ESTADO >>>'
go


