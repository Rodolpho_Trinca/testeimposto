USE [Teste]
GO

IF OBJECT_ID('dbo.P_DELETAR_ESTADO') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.P_DELETAR_ESTADO
    IF OBJECT_ID('dbo.P_DELETAR_ESTADO') IS NOT NULL
        PRINT '<<< FALHA APAGANDO A PROCEDURE dbo.P_DELETAR_ESTADO >>>'
    ELSE
        PRINT '<<< PROCEDURE dbo.P_DELETAR_ESTADO APAGADA >>>'
END
go
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON 
GO 
CREATE PROCEDURE dbo.P_DELETAR_ESTADO 
(
	@pId int
)
AS
BEGIN
	IF (@pId != 0 OR @pId IS NOT NULL)
		BEGIN 
			DELETE FROM
				[dbo].[Estados]
			WHERE
				Id = @pId
		END    
END
GO
GRANT EXECUTE ON dbo.P_DELETAR_ESTADO TO [public]
GO
IF OBJECT_ID('dbo.P_DELETAR_ESTADO') IS NOT NULL
    PRINT '<<< PROCEDURE dbo.P_DELETAR_ESTADO CRIADA >>>'
ELSE
    PRINT '<<< FALHA NA CRIACAO DA PROCEDURE dbo.P_DELETAR_ESTADO >>>'
GO


