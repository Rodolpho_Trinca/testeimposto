USE [Teste]
GO
IF OBJECT_ID('dbo.P_CONSULTA_ESTADO') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.P_CONSULTA_ESTADO
    IF OBJECT_ID('dbo.P_CONSULTA_ESTADO') IS NOT NULL
        PRINT '<<< FALHA APAGANDO A PROCEDURE dbo.P_CONSULTA_ESTADO >>>'
    ELSE
        PRINT '<<< PROCEDURE dbo.P_CONSULTA_ESTADO APAGADA >>>'
END
go
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON 
GO 
CREATE PROCEDURE dbo.P_CONSULTA_ESTADO 
(
	@pId int = 0
)
AS
BEGIN
	IF (@pId = 0)
		BEGIN 
			SELECT
				[Id]
				,[NomeEstado]
				,[Sigla]
				,[CFOP]
				,[CentroDistribuicao]
				,[EntregaDisponivel]
			FROM
				[dbo].[Estados]
		END
	ELSE
		BEGIN
			SELECT
				[Id]
				,[NomeEstado] 
				,[Sigla]
				,[CFOP]
				,[CentroDistribuicao]
				,[EntregaDisponivel]
			FROM
				[dbo].[Estados]
			WHERE Id = @pId
		END	    
END
GO
GRANT EXECUTE ON dbo.P_CONSULTA_ESTADO TO [public]
GO
IF OBJECT_ID('dbo.P_CONSULTA_ESTADO') IS NOT NULL
    PRINT '<<< PROCEDURE dbo.P_CONSULTA_ESTADO CRIADA >>>'
ELSE
    PRINT '<<< FALHA NA CRIACAO DA PROCEDURE dbo.P_CONSULTA_ESTADO >>>'
GO


