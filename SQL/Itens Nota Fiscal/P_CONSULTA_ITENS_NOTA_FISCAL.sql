USE [Teste]
GO
IF OBJECT_ID('dbo.P_CONSULTA_ITENS_NOTA_FISCAL') IS NOT NULL
BEGIN
    DROP PROCEDURE dbo.P_CONSULTA_ITENS_NOTA_FISCAL
    IF OBJECT_ID('dbo.P_CONSULTA_ITENS_NOTA_FISCAL') IS NOT NULL
        PRINT '<<< FALHA APAGANDO A PROCEDURE dbo.P_CONSULTA_ITENS_NOTA_FISCAL >>>'
    ELSE
        PRINT '<<< PROCEDURE dbo.P_CONSULTA_ITENS_NOTA_FISCAL APAGADA >>>'
END
go
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON 
GO 
CREATE PROCEDURE dbo.P_CONSULTA_ITENS_NOTA_FISCAL 
(
	@pIdNotaFiscal int
)
AS
BEGIN
	SELECT 
		[IdNotaFiscal]
        ,[Cfop]
        ,[TipoIcms]
        ,[BaseIcms]
        ,[AliquotaIcms]
        ,[ValorIcms]
        ,[BaseIpi]
        ,[AliquotaIpi]
        ,[ValorIpi]
        ,[NomeProduto]
        ,[CodigoProduto]
		,[ValorProduto]
		,[ValorDesconto]
        ,[EstadoOrigem]
        ,[EstadoDestino]
		,[Brinde]
	FROM
		[dbo].[NotaFiscalItem]
	WHERE IdNotaFiscal = @pIdNotaFiscal
END
GO
GRANT EXECUTE ON dbo.P_CONSULTA_ITENS_NOTA_FISCAL TO [public]
GO
IF OBJECT_ID('dbo.P_CONSULTA_ITENS_NOTA_FISCAL') IS NOT NULL
    PRINT '<<< PROCEDURE dbo.P_CONSULTA_ITENS_NOTA_FISCAL CRIADA >>>'
ELSE
    PRINT '<<< FALHA NA CRIACAO DA PROCEDURE dbo.P_CONSULTA_ITENS_NOTA_FISCAL >>>'
GO


