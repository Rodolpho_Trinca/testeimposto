﻿using System;
using Imposto.Core.Domain;
using Imposto.Core.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Imposto.Testes
{
    [TestClass]
    public class TesteConfigs
    {
        [TestMethod]
        public void AlterarCaminhoSaida()
        {
            ConfigService service = new ConfigService();

            SystemConfig config = service.Consultar();

            string valorDefinido = "";
            string nomeCampo = "CaminhoSalvarNFE";
            Config caminhoSalvarNFE;
            if (!config.ExisteCampo(nomeCampo))
            {
                config.Configs.Add(new Config() { Campo = nomeCampo, Valor = Environment.CurrentDirectory });
            }
            else
            {
                caminhoSalvarNFE = config.BuscaConfig(nomeCampo);
                valorDefinido = caminhoSalvarNFE.Valor;
                caminhoSalvarNFE.Valor = Environment.CurrentDirectory;
            }

            service.Salvar(config);

            caminhoSalvarNFE = config.BuscaConfig(nomeCampo);
            if (string.IsNullOrEmpty(valorDefinido))
            {
                Assert.AreEqual(Environment.CurrentDirectory, caminhoSalvarNFE.Valor);
            }

            caminhoSalvarNFE.Valor = valorDefinido;

            service.Salvar(config);
        }
    }
}
