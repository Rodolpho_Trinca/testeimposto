﻿using System;
using Imposto.Core.Domain;
using Imposto.Core.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Imposto.Testes
{
    [TestClass]
    public class TesteLogin
    {
        [TestMethod]
        public void UsuarioInvalido()
        {
            Usuario usuario = new Usuario();
            usuario.User = "asdfg";
            usuario.ConverterSenhaHash("dsadsadsa");
            try
            {

                LoginService service = new LoginService();
                service.LogarUsuario(usuario);
            }
            catch (Exception ex)
            {
                Assert.AreEqual("Usuario não encontrado", ex.Message);
            }
        }

        [TestMethod]
        public void UsuarioValido()
        {
            Usuario usuario = new Usuario();
            usuario.User = "Admin";
            usuario.ConverterSenhaHash("123456");
            
            LoginService service = new LoginService();
            Assert.IsTrue(service.LogarUsuario(usuario));
        }
    }
}
