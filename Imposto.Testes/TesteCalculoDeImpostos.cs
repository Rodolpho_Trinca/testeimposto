﻿using System;
using System.Linq;
using Imposto.Core.Domain;
using Imposto.Core.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Imposto.Testes
{
    [TestClass]
    public class TesteCalculoDeImpostos
    {
        #region ICMS

        [TestMethod]
        public void ConfereAlquotaICMSBrinde()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.Brinde = true;

            Assert.AreEqual(0.18m, item.ICMS.AliquotaIcms);
        }

        [TestMethod]
        public void ConfereAlquotaICMSMesmoEstado()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosOrigem().First();

            Assert.AreEqual(0.18m, item.ICMS.AliquotaIcms);
        }

        [TestMethod]
        public void ConfereAlquotaICMSEstadosDiferentes()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(0.17m, item.ICMS.AliquotaIcms);
        }

        [TestMethod]
        public void ConfereTipoICMSBrinde()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.Brinde = true;

            Assert.AreEqual("60", item.ICMS.TipoIcms);
        }

        [TestMethod]
        public void ConfereTipoICMSMesmoEstado()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosOrigem().First();

            Assert.AreEqual("60", item.ICMS.TipoIcms);
        }

        [TestMethod]
        public void ConfereTipoICMSEstadosDiferentes()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();

            Assert.AreEqual("10", item.ICMS.TipoIcms);
        }

        [TestMethod]
        public void ConfereBaseICMS6009()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoDestino = estadoService.ListarEstadosDestino().First(e => e.Sigla == "SC");
            item.ValorProduto = 150.0m;

            Assert.AreEqual(135.0m, item.ICMS.BaseIcms);
        }

        [TestMethod]
        public void ConfereBaseICMS()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(150.0m, item.ICMS.BaseIcms);
        }

        [TestMethod]
        public void ConfereValorICMSReducaoBase6009()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First(e => e.Sigla == "SC");
            item.ValorProduto = 150.0m;

            Assert.AreEqual(22.95m, item.ICMS.Calcula());
        }

        [TestMethod]
        public void ConfereValorICMS()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosDestino().First(e => e.Sigla == "SC");
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(25.5m, item.ICMS.Calcula());
        }

        [TestMethod]
        public void ConfereValorICMSAumentaAliquota()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosDestino().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(27.0m, item.ICMS.Calcula());
        }

        #endregion

        #region IPI
        [TestMethod]
        public void ConfereValorIPI()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(15.0m, item.IPI.Calcula());
        }

        [TestMethod]
        public void ConfereValorIPIProdutoBrinde()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;
            item.Brinde = true;

            Assert.AreEqual(0m, item.IPI.Calcula());
        }

        [TestMethod]
        public void ConfereAliquotaIPIProdutoBrinde()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.Brinde = true;

            Assert.AreEqual(0m, item.IPI.AliquotaIpi);
        }

        [TestMethod]
        public void ConfereAliquotaIPI()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoOrigem = estadoService.ListarEstadosOrigem().First();
            item.EstadoDestino = estadoService.ListarEstadosDestino().First();

            Assert.AreEqual(0.10m, item.IPI.AliquotaIpi);
        }

        [TestMethod]
        public void ConfereBaseIPI()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoDestino = estadoService.ListarEstadosDestino().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(150.0m, item.IPI.BaseIpi);
        }
        #endregion
    }
}
