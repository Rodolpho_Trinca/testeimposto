﻿using System;
using System.IO;
using System.Linq;
using Imposto.Core.Domain;
using Imposto.Core.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Imposto.Testes
{
    [TestClass]
    public class TesteNotaFiscal
    {
        [TestMethod]
        public void SalvarNotaFiscal()
        {
            NotaFiscalService notaFiscalService = new NotaFiscalService();
            EstadoService estadoService = new EstadoService();

            Pedido pedido = new Pedido()
            {
                EstadoOrigem = estadoService.ListarEstadosOrigem().First(),
                EstadoDestino = estadoService.ListarEstadosDestino().First(),
                NomeCliente = "Cliente de teste de unidade"
            };
            pedido.ItensDoPedido.Add(new PedidoItem()
            {
                Brinde = false,
                CodigoProduto = "123456",
                NomeProduto = "brinde de teste de unidade",
                ValorItemPedido = 150.00m
            });

            pedido.ItensDoPedido.Add(new PedidoItem()
            {
                Brinde = false,
                CodigoProduto = "987654",
                NomeProduto = "Produto de teste de unidade",
                ValorItemPedido = 450.00m
            });

            notaFiscalService.GerarNotaFiscal(pedido);
        }

        [TestMethod]
        public void ConfereDesconto()
        {
            NotaFiscalItem item = new NotaFiscalItem();
            EstadoService estadoService = new EstadoService();

            item.EstadoDestino = estadoService.ListarEstadosOrigem().First();
            item.ValorProduto = 150.0m;

            Assert.AreEqual(15m, item.Desconto.ValorDesconto);
        }
    }
}
