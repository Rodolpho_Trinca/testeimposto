﻿using Imposto.Core.Data;
using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Service
{
    public class EstadoService
    {
        public List<Estado> ListarEstados()
        {
            EstadoRepository repo = new EstadoRepository();
            return repo.Consultar();
        }

        public List<Estado> ListarEstadosOrigem()
        {
            EstadoRepository repo = new EstadoRepository();
            return repo.Consultar().Where(e => e.CentroDistribuicao == true).ToList();
        }

        public List<Estado> ListarEstadosDestino()
        {
            EstadoRepository repo = new EstadoRepository();
            return repo.Consultar().Where(e => e.EntregaDisponivel == true).ToList();
        }
    }
}
