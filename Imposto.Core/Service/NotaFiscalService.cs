﻿using Imposto.Core.Data;
using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imposto.Core.Service
{
    public class NotaFiscalService
    {
        public void GerarNotaFiscal(Domain.Pedido pedido)
        {
            NotaFiscal notaFiscal = new NotaFiscal();
            notaFiscal.EmitirNotaFiscal(pedido);
            SalvarXML(notaFiscal);
            NotaFiscalRepository repo = new NotaFiscalRepository();
            repo.Salvar(notaFiscal);
        }

        private void SalvarXML(NotaFiscal notaFiscal)
        {
            SystemConfigRepository repo = new SystemConfigRepository();
            var config = repo.Consultar();
            string caminhoArquivo = Environment.CurrentDirectory;
            string nomeCampo = "CaminhoSalvarNFE";

            if (config.ExisteCampo(nomeCampo))
            {
               caminhoArquivo = config.BuscaConfig(nomeCampo).Valor;
            }

            string nomeArquivoNota = $"NFE_{notaFiscal.NumeroNotaFiscal}_{notaFiscal.DataEmissao.ToString("dd_MM_yyyy_HH_mm")}.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(NotaFiscal));

            using (TextWriter writer = new StreamWriter(Path.Combine(caminhoArquivo, nomeArquivoNota)))
            {
                serializer.Serialize(writer, notaFiscal);
            }
        }
    }
}
