﻿using Imposto.Core.Data;
using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Service
{
    public class LoginService
    {
        public void Salvar(Usuario ent)
        {
            LoginRepository repo = new LoginRepository();
            repo.Salvar(ent);
        }

        public List<Usuario> Consultar()
        {
            LoginRepository repo = new LoginRepository();
            return repo.Consultar();
        }

        public Usuario Consultar(string usuario)
        {
            LoginRepository repo = new LoginRepository();
            return repo.Consultar()
                        .FirstOrDefault(u => u.User == usuario);
        }

        public bool LogarUsuario(Usuario usuario)
        {
            LoginRepository repo = new LoginRepository();
            var usuarioDb = repo.ConsultarUsuario(usuario.User);
            return usuarioDb.LogarUsuario(usuario);
        }
    }
}
