﻿using Imposto.Core.Data;
using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Service
{
    public class ConfigService
    {
        public void Salvar(SystemConfig ent)
        {
            SystemConfigRepository repo = new SystemConfigRepository();
            repo.Salvar(ent);
        }

        public SystemConfig Consultar()
        {
            SystemConfigRepository repo = new SystemConfigRepository();
            return repo.Consultar();
        }

        public Config Consultar(string campo)
        {
            SystemConfigRepository repo = new SystemConfigRepository();
            return repo.Consultar()
                        .BuscaConfig(campo);
        }

        //public bool LogarUsuario(Usuario usuario)
        //{
        //    LoginRepository repo = new LoginRepository();
        //    var usuarioDb = repo.ConsultarUsuario(usuario.User);
        //    return usuarioDb.LogarUsuario(usuario);
        //}
    }
}
