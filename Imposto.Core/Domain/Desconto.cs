﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class Desconto
    {
        public decimal ValorDesconto
        {
            get {
                var desconto = item.ValorProduto;
                if (estadosDesconto.Contains(item.EstadoDestino.Sigla))
                {
                    desconto = item.ValorProduto * 0.10m; //10 % desconto
                }

                return desconto;
            } set { }
        }

        private string[] estadosDesconto = new string[]{"SP", "RJ", "MG", "ES"};
        private NotaFiscalItem item;

        public Desconto(NotaFiscalItem item)
        {
            this.item = item;
        }
        public Desconto()
        {
                
        }
    }
}
