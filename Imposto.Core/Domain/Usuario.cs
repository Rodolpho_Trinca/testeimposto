﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class Usuario
    {
        public int Id { get; set; }
        public string User { get; set; }
        public string Senha { get; set; }

        public void ConverterSenhaHash(string senha)
        {
            this.Senha = GetHashString(senha);
        }

        public bool LogarUsuario(Usuario usuario) => (usuario.User == User) && (usuario.Senha == Senha);

        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
