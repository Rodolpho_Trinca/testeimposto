﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class Estado
    {
        public int Id { get; set; }
        public string NomeEstado { get; set; }
        public string CFOP { get; set; }
        public string Sigla { get; set; }
        public bool CentroDistribuicao { get; set; }
        public bool EntregaDisponivel { get; set; }

        public override string ToString()
        {
            return NomeEstado;
        }

        public override bool Equals(object obj)
        {
            if (obj is null || obj.GetType() != this.GetType())
                return false;

            Estado objEstado = (Estado)obj;
            if (objEstado is null)
                return false;

            return this.Sigla.Equals(objEstado.Sigla);
        }
    }
}
