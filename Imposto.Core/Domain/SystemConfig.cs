﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class SystemConfig
    {
        public List<Config> Configs { get; set; }
        public SystemConfig()
        {
            Configs = new List<Config>();
        }

        public bool ExisteCampo(string nomeCampo)
        {
            return (Configs != null) && (Configs.FirstOrDefault(c => c.Campo == nomeCampo) != null);
        }

        public Config BuscaConfig(string nomeCampo)
        {
            if (Configs == null)
                return new Config();

            return Configs.First(c => c.Campo == nomeCampo);
        }
    }
}
