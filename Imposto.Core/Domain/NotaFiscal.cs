﻿using Imposto.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Imposto.Core.Domain
{
    [XmlRootAttribute("NotaFiscal", IsNullable = false)]
    public class NotaFiscal
    {
        public int Id { get; set; }
        public int NumeroNotaFiscal { get; set; }
        public int Serie { get; set; }
        public string NomeCliente { get; set; }

        public Estado EstadoDestino { get; set; }
        public Estado EstadoOrigem { get; set; }
        public DateTime DataEmissao { get; set; }

        //Alterado IEnumerable para List
        public List<NotaFiscalItem> ItensDaNotaFiscal { get; set; }

        public NotaFiscal()
        {
            ItensDaNotaFiscal = new List<NotaFiscalItem>();
        }

        public void EmitirNotaFiscal(Pedido pedido)
        {
            NotaFiscalRepository notaRepo = new NotaFiscalRepository();

            this.NumeroNotaFiscal = notaRepo.ConsultarUltimoNumero() + 1;
            this.Serie = new Random().Next(Int32.MaxValue);
            this.NomeCliente = pedido.NomeCliente;

            this.EstadoDestino = pedido.EstadoOrigem;
            this.EstadoOrigem = pedido.EstadoDestino;

            this.DataEmissao = DateTime.Now;

            #region Calculo do imposto
            foreach (PedidoItem itemPedido in pedido.ItensDoPedido)
            {
                //Melhorar esse sistema
                NotaFiscalItem notaFiscalItem = new NotaFiscalItem();
                notaFiscalItem.MontaItem(itemPedido, this.EstadoOrigem, this.EstadoDestino, this.Id);

                //notaFiscalItem.Cfop = this.EstadoDestino.CFOP;
                //notaFiscalItem.NomeProduto = itemPedido.NomeProduto;
                //notaFiscalItem.CodigoProduto = itemPedido.CodigoProduto;
                //notaFiscalItem.ValorProduto = itemPedido.ValorItemPedido;
                //notaFiscalItem.Brinde = itemPedido.Brinde;

                //if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "RJ"))
                //{
                //    notaFiscalItem.Cfop = "6.000";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "PE"))
                //{
                //    notaFiscalItem.Cfop = "6.001";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "MG"))
                //{
                //    notaFiscalItem.Cfop = "6.002";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "PB"))
                //{
                //    notaFiscalItem.Cfop = "6.003";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "PR"))
                //{
                //    notaFiscalItem.Cfop = "6.004";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "PI"))
                //{
                //    notaFiscalItem.Cfop = "6.005";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "RO"))
                //{
                //    notaFiscalItem.Cfop = "6.006";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "SE"))
                //{
                //    notaFiscalItem.Cfop = "6.007";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "TO"))
                //{
                //    notaFiscalItem.Cfop = "6.008";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "SE"))
                //{
                //    notaFiscalItem.Cfop = "6.009";
                //}
                //else if ((this.EstadoOrigem == "SP") && (this.EstadoDestino == "PA"))
                //{
                //    notaFiscalItem.Cfop = "6.010";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "RJ"))
                //{
                //    notaFiscalItem.Cfop = "6.000";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "PE"))
                //{
                //    notaFiscalItem.Cfop = "6.001";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "MG"))
                //{
                //    notaFiscalItem.Cfop = "6.002";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "PB"))
                //{
                //    notaFiscalItem.Cfop = "6.003";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "PR"))
                //{
                //    notaFiscalItem.Cfop = "6.004";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "PI"))
                //{
                //    notaFiscalItem.Cfop = "6.005";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "RO"))
                //{
                //    notaFiscalItem.Cfop = "6.006";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "SE"))
                //{
                //    notaFiscalItem.Cfop = "6.007";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "TO"))
                //{
                //    notaFiscalItem.Cfop = "6.008";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "SE"))
                //{
                //    notaFiscalItem.Cfop = "6.009";
                //}
                //else if ((this.EstadoOrigem == "MG") && (this.EstadoDestino == "PA"))
                //{
                //    notaFiscalItem.Cfop = "6.010";
                //}

                //Melhorar o sistema de calculo de imposto
                //notaFiscalItem.CalculaImposto(this.EstadoOrigem, this.EstadoDestino);

                //if (this.EstadoDestino == this.EstadoOrigem)
                //{
                //    notaFiscalItem.TipoIcms = "60";
                //    notaFiscalItem.AliquotaIcms = 0.18m;
                //}
                //else
                //{
                //    notaFiscalItem.TipoIcms = "10";
                //    notaFiscalItem.AliquotaIcms = 0.17m;
                //}
                //if (notaFiscalItem.Cfop == "6.009")
                //{
                //    notaFiscalItem.BaseIcms = itemPedido.ValorItemPedido * 0.90m; //redução de base
                //}
                //else
                //{
                //    notaFiscalItem.BaseIcms = itemPedido.ValorItemPedido;
                //}
                //notaFiscalItem.ValorIcms = notaFiscalItem.BaseIcms * notaFiscalItem.AliquotaIcms;

                //if (itemPedido.Brinde)
                //{
                //    notaFiscalItem.TipoIcms = "60";
                //    notaFiscalItem.AliquotaIcms = 0.18m;
                //    notaFiscalItem.ValorIcms = notaFiscalItem.BaseIcms * notaFiscalItem.AliquotaIcms;
                //}
                
                ItensDaNotaFiscal.Add(notaFiscalItem);
            }
            #endregion
        }
    }
}
