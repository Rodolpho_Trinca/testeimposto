﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class Config
    {
        public int Id { get; set; }
        public string Campo { get; set; }
        public string Valor { get; set; }
    }
}
