﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain.Impostos
{
    public class IPI : Imposto
    {
        public decimal BaseIpi { get { return Base(); } set { } }
        public decimal AliquotaIpi { get { return Aliquota(); } set { } }
        public decimal ValorIpi { get { return Calcula(); } set { } }

        public IPI(NotaFiscalItem item) : base(item)
        {
        }

        public IPI()
        {

        }

        public override decimal Aliquota()
        {
            if (item == null)
                throw new Exception("Não há item para calcular o imposto");

            if (item.Brinde)
                return 0m;
            else
                return 0.10m;
        }

        public override decimal Base()
        {
            if (item == null)
                throw new Exception("Não há item para calcular o imposto");

            return item.ValorProduto;
        }
    }
}
