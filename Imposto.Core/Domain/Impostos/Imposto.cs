﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain.Impostos
{
    public abstract class Imposto
    {
        protected NotaFiscalItem item;

        public Imposto(NotaFiscalItem item)
        {
            this.item = item;
        }
        public Imposto()
        {

        }
        public abstract decimal Aliquota();
        public abstract decimal Base();
        public virtual decimal Calcula()
        {
            return Base() * Aliquota();
        }
    }
}
