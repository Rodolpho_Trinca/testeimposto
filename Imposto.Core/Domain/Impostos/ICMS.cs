﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain.Impostos
{
    public class ICMS : Imposto
    {
        public decimal BaseIcms { get { return Base(); } set { } }
        public decimal AliquotaIcms { get { return Aliquota(); } set { } }
        public string TipoIcms { get { return Tipo(); } set { } }
        public decimal ValorIcms { get { return Calcula(); } set { } }

        public ICMS(NotaFiscalItem item) : base(item) {}
        public ICMS()
        {

        }

        public override decimal Aliquota()
        {
            if (item == null)
                throw new Exception("Não há item para calcular o imposto");

            if (item.EstadoOrigem.Equals(item.EstadoDestino) || item.Brinde)
            {
                return 0.18m;
            }
            else
            {
                return 0.17m;
            }
        }

        public override decimal Base()
        {
            if (item == null)
                throw new Exception("Não há item para calcular o imposto");

            if (item.EstadoDestino.CFOP == "6.009")
            {
                return item.ValorProduto * 0.90m;
            }
            else
            {
                return item.ValorProduto* 1.00m;
            }
        }

        public string Tipo()
        {
            if (item == null)
                throw new Exception("Não há item para calcular o imposto");

            if (item.EstadoOrigem.Equals(item.EstadoDestino) || item.Brinde)
            {
                return "60";
            }
            else
            {
                return "10";
            }
        }
    }
}
