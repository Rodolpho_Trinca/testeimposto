﻿using Imposto.Core.Domain.Impostos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Domain
{
    public class NotaFiscalItem
    {
        public int Id { get; set; }
        public int IdNotaFiscal { get; set; }
        public string Cfop { get; set; }
        public string NomeProduto { get; set; }
        public string CodigoProduto { get; set; }
        public bool Brinde { get; set; }
        public decimal ValorProduto { get; set; }
        public Estado EstadoOrigem { get; set; }
        public Estado EstadoDestino { get; set; }

        public Desconto Desconto { get; set; }

        public ICMS ICMS { get; set; }
        public IPI IPI { get; set; }

        public NotaFiscalItem()
        {
            IPI = new IPI(this);
            ICMS = new ICMS(this);
            Desconto = new Desconto(this);
        }
        
        public void MontaItem(PedidoItem pedidoItem, Estado origem, Estado destino, int idNotaFiscal)
        {
            this.EstadoDestino = destino;
            this.EstadoOrigem = origem;
            this.Cfop = EstadoDestino.CFOP;
            this.NomeProduto = pedidoItem.NomeProduto;
            this.CodigoProduto = pedidoItem.CodigoProduto;
            this.ValorProduto = pedidoItem.ValorItemPedido;
            this.Brinde = pedidoItem.Brinde;
            this.IdNotaFiscal = idNotaFiscal;
        }

        
    }
}
                                                    