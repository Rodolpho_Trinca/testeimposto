﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    internal interface IRepository<T>
    {
        int Salvar(T ent);
        T ConsultarPorId(int id);
        List<T> Consultar(int id = 0);
        void Excluir(int id);
    }
}
