﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    internal class EstadoRepository : Conexao, IRepository<Estado>
    {
        public int Salvar(Estado ent)
        {
            SqlParameter[] parametros = new SqlParameter[]
           {
                new SqlParameter("pId", ent.Id),
                new SqlParameter("pNomeEstado", ent.NomeEstado),
                new SqlParameter("pSigla", ent.Sigla),
                new SqlParameter("pCFOP", ent.CFOP),
                new SqlParameter("pCentroDistribuicao", ent.CentroDistribuicao),
                new SqlParameter("pEntregaDisponivel", ent.EntregaDisponivel),
           };

            DataSet ds = base.Consultar("dbo.P_INCLUIR_ATUALIZAR_ESTADO", parametros);
            int ret = 0;
            if (ds.Tables[0].Rows.Count > 0)
                int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out ret);

            return ret;
        }

        public Estado ConsultarPorId(int id) => Consultar(id).First();

        public List<Estado> Consultar(int id = 0)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            DataSet nf = this.Consultar("dbo.P_CONSULTA_ESTADO", parametros);

            return Converter(nf);
        }

        public Estado ConsultarSigla(string sigla) => Consultar().FirstOrDefault(e => e.Sigla == sigla);

        public void Excluir(int id)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            base.Executar("dbo.P_DELETAR_ESTADO", parametros);
        }

        private List<Estado> Converter(DataSet estados)
        {
            List<Estado> lista = new List<Estado>();
            if (estados == null && estados.Tables == null && estados.Tables.Count == 0 && estados.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Data Set estados não é valido");
            }

            foreach (DataRow row in estados.Tables[0].Rows)
            {
                Estado estado = new Estado()
                {
                    Id = row.Field<int>("Id"),
                    CFOP = row.Field<string>("CFOP"),
                    CentroDistribuicao = row.Field<bool>("CentroDistribuicao"),
                    EntregaDisponivel = row.Field<bool>("EntregaDisponivel"),
                    NomeEstado = row.Field<string>("NomeEstado"),
                    Sigla = row.Field<string>("Sigla")
                };

                lista.Add(estado);
            }

            return lista;
        }

        
    }
}
