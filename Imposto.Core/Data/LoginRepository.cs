﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    public class LoginRepository : Conexao, IRepository<Usuario>
    {
        public List<Usuario> Consultar(int id = 0)
        {
            SqlParameter[] parametros = new SqlParameter[]
           {
                new SqlParameter("pId", id),
           };

            DataSet usuarios = base.Consultar("dbo.P_CONSULTA_USUARIO", parametros);
            return Converter(usuarios);
        }

        public Usuario ConsultarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public Usuario ConsultarUsuario(string usuario)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pUsuario", usuario),
            };

            DataSet ds = base.Consultar("dbo.P_LOGAR_USUARIO", parametros);

            if (ds.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Usuario não encontrado");
            }

            return Converter(ds).First();
        }

        public void Excluir(int id)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            base.Executar("dbo.P_DELETAR_USUARIO", parametros);
        }

        public int Salvar(Usuario ent)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", ent.Id),
                new SqlParameter("pUsuario", ent.User),
                new SqlParameter("pSenha", ent.Senha),
            };

            DataSet ds = base.Consultar("dbo.P_INCLUIR_ATUALIZAR_USUARIO", parametros);
            int ret = 0;
            if (ds.Tables[0].Rows.Count > 0)
                int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out ret);

            return ret;
        }

        private List<Usuario> Converter(DataSet dsConfig)
        {
            if (dsConfig == null && dsConfig.Tables == null && dsConfig.Tables.Count == 0 && dsConfig.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Data Set Notas não é valido");
            }

            List<Usuario> lista = new List<Usuario>();
            foreach (DataRow row in dsConfig.Tables[0].Rows)
            {
                Usuario usuario = new Usuario()
                {
                    Id = row.Field<int>("Id"),
                    User = row.Field<string>("Usuario"),
                    Senha = row.Field<string>("Senha"),
                };

                lista.Add(usuario);
            }

            return lista;
        }
    }
}
