﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    public class Conexao
    {
        protected SqlConnection conexao;

        public Conexao()
        {
            string connetionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Teste;Integrated Security=SSPI;";
            //string connetionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Teste;User ID=sa;Password=demol23";
            conexao = new SqlConnection(connetionString);
        }
        

        internal void Executar(string NomeProcedure, SqlParameter[] parametros)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexao;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = NomeProcedure;
            foreach (var item in parametros)
                comando.Parameters.Add(item);

            conexao.Open();
            try
            {
                comando.ExecuteNonQuery();
            }
            finally
            {
                conexao.Close();
            }
        }

        internal DataSet Consultar(string NomeProcedure, SqlParameter[] parametros)
        {
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexao;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = NomeProcedure;
            foreach (var item in parametros)
                comando.Parameters.Add(item);

            SqlDataAdapter adapter = new SqlDataAdapter(comando);
            DataSet ds = new DataSet();
            conexao.Open();

            try
            {
                adapter.Fill(ds);
            }
            finally
            {
                conexao.Close();
            }

            return ds;
        }
    }
}
