﻿using Imposto.Core.Domain;
using Imposto.Core.Domain.Impostos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    internal class ItensNotaFiscalRepository : Conexao, IRepository<NotaFiscalItem>
    {
        public int Salvar(NotaFiscalItem ent)
        {
            SqlParameter[] parametros = new SqlParameter[]
           {
                new SqlParameter("pId", ent.Id),
                new SqlParameter("pIdNotaFiscal", ent.IdNotaFiscal),
                new SqlParameter("pCfop", ent.Cfop),
                new SqlParameter("pTipoIcms", ent.ICMS.Tipo()),
                new SqlParameter("pBaseIcms", ent.ICMS.Base()),
                new SqlParameter("pAliquotaIcms", ent.ICMS.Aliquota()),
                new SqlParameter("pValorIcms", ent.ICMS.Calcula()),
                new SqlParameter("pBaseIpi", ent.IPI.Base()),
                new SqlParameter("pAliquotaIpi", ent.IPI.Aliquota()),
                new SqlParameter("pValorIpi", ent.IPI.Calcula()),
                new SqlParameter("pNomeProduto", ent.NomeProduto),
                new SqlParameter("pCodigoProduto", ent.CodigoProduto),
                new SqlParameter("pValorProduto", ent.ValorProduto),
                new SqlParameter("pValorDesconto", ent.Desconto.ValorDesconto),
                new SqlParameter("pEstadoOrigem", ent.EstadoOrigem.Sigla),
                new SqlParameter("pEstadoDestino", ent.EstadoDestino.Sigla),
                new SqlParameter("pBrinde", ent.Brinde)
           };

            DataSet ds = base.Consultar("dbo.P_INCLUIR_ATUALIZAR_NOTA_FISCAL_ITEM", parametros);
            int ret = 0;
            if (ds.Tables[0].Rows.Count > 0)
                int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out ret);

            return ret;
        }

        public NotaFiscalItem ConsultarPorId(int id) => Consultar(id).First();

        public List<NotaFiscalItem> Consultar(int id = 0)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            DataSet nfItens = this.Consultar("dbo.P_CONSULTA_NOTA_FISCAL_ITEM", parametros);

            return ConverterItens(nfItens);
        }

        public void Excluir(int id)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            base.Executar("dbo.P_EXCLUIR_NOTA_FISCAL_ITEM", parametros);
        }

        internal List<NotaFiscalItem> BuscaItensNotaFiscal(int id)
        {
            if (id == 0)
                throw new ArgumentException("Por favor, digite um id valido");

            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pIdNotaFiscal", id)
            };

            DataSet nfItens = base.Consultar("dbo.P_CONSULTA_ITENS_NOTA_FISCAL", parametros);
            return ConverterItens(nfItens);
        }

        private List<NotaFiscalItem> ConverterItens(DataSet dataSetItensNota)
        {
            List<NotaFiscalItem> lista = new List<NotaFiscalItem>();
            if (dataSetItensNota == null && dataSetItensNota.Tables == null && dataSetItensNota.Tables.Count == 0 && dataSetItensNota.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Data Set Itens Nota não é valido");
            }

            EstadoRepository repo = new EstadoRepository();
            foreach (DataRow rowItens in dataSetItensNota.Tables[0].Rows)
            {
                NotaFiscalItem nfItem = new NotaFiscalItem()
                {
                    Id = rowItens.Field<int>("Id"),
                    IdNotaFiscal = rowItens.Field<int>("IdNotaFiscal"),
                    Cfop = rowItens.Field<string>("Cfop"),
                    NomeProduto = rowItens.Field<string>("NomeProduto"),
                    CodigoProduto = rowItens.Field<string>("CodigoProduto"),
                    EstadoDestino = repo.ConsultarSigla(rowItens.Field<string>("EstadoDestino")),
                    EstadoOrigem = repo.ConsultarSigla(rowItens.Field<string>("EstadoOrigem")),
                    Brinde = rowItens.Field<bool>("Brinde"),
                    ValorProduto = rowItens.Field<decimal>("ValorProduto")
                };

                nfItem.ICMS = new ICMS(nfItem);
                nfItem.IPI = new IPI(nfItem);
                nfItem.Desconto = new Desconto(nfItem);

                lista.Add(nfItem);
            }

            return lista;
        }
    }
}
