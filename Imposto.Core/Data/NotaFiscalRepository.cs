﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    internal class NotaFiscalRepository : Conexao, IRepository<NotaFiscal>
    {
        public int Salvar(NotaFiscal ent)
        {
            SqlParameter[] parametros = new SqlParameter[]
           {
                new SqlParameter("pId", ent.Id),
                new SqlParameter("pNumeroNotaFiscal", ent.NumeroNotaFiscal),
                new SqlParameter("pDataEmissao", ent.DataEmissao),
                new SqlParameter("pSerie", ent.Serie),
                new SqlParameter("pNomeCliente", ent.NomeCliente),
                new SqlParameter("pEstadoDestino", ent.EstadoDestino.Sigla),
                new SqlParameter("pEstadoOrigem", ent.EstadoOrigem.Sigla)
           };

            DataSet ds = base.Consultar("dbo.P_INCLUIR_ATUALIZAR_NOTA_FISCAL", parametros);
            int ret = 0;
            if (ds.Tables[0].Rows.Count > 0)
                int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out ret);

            ItensNotaFiscalRepository itensRepo = new ItensNotaFiscalRepository();
            foreach (var item in ent.ItensDaNotaFiscal)
            {
                item.IdNotaFiscal = ret;
                itensRepo.Salvar(item);
            }

            return ret;
        }

        public NotaFiscal ConsultarPorId(int id) => Consultar(id).First();

        public List<NotaFiscal> Consultar(int id = 0)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            DataSet nf = this.Consultar("dbo.P_CONSULTA_NOTA_FISCAL", parametros);
            DataSet nfItens = this.Consultar("dbo.P_CONSULTA_NOTA_FISCAL", parametros);

            return ConverterNotasFiscais(nf, nfItens);
        }

        public int ConsultarUltimoNumero()
        {
            SqlParameter[] parametros = new SqlParameter[] {};

            DataSet ds = base.Consultar("dbo.P_CONSULTA_ULTIMO_NUMERO_NOTA_FISCAL", parametros);
            int ret = 0;
            if (ds.Tables[0].Rows.Count > 0)
                int.TryParse(ds.Tables[0].Rows[0][0].ToString(), out ret);

            return ret;
        }

        public void Excluir(int id)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            base.Executar("dbo.P_EXCLUIR_NOTA_FISCAL", parametros);
        }

        private List<NotaFiscal> ConverterNotasFiscais(DataSet dataSetNotas, DataSet dataSetItensNota = null)
        {
            List<NotaFiscal> lista = new List<NotaFiscal>();
            if (dataSetNotas == null && dataSetNotas.Tables == null && dataSetNotas.Tables.Count == 0 && dataSetNotas.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Data Set Notas não é valido");
            }

            ItensNotaFiscalRepository itensRepo = new ItensNotaFiscalRepository();
            EstadoRepository estadoRepo = new EstadoRepository();
            foreach (DataRow row in dataSetNotas.Tables[0].Rows)
            {
                NotaFiscal nf = new NotaFiscal()
                {
                    Id = row.Field<int>("Id"),
                    DataEmissao = row.Field<DateTime>("DataEmissao"),
                    NomeCliente = row.Field<string>("NomeCliente"),
                    NumeroNotaFiscal = row.Field<int>("NumeroNotaFiscal"),
                    Serie = row.Field<int>("Serie")
                };

                nf.EstadoDestino = estadoRepo.Consultar().Where(e => e.Sigla == row.Field<string>("EstadoDestino")).FirstOrDefault();
                nf.EstadoOrigem = estadoRepo.Consultar().Where(e => e.Sigla == row.Field<string>("EstadoOrigem")).FirstOrDefault();

                nf.ItensDaNotaFiscal = itensRepo.BuscaItensNotaFiscal(nf.Id);

                lista.Add(nf);
            }

            return lista;
        }
    }
}
