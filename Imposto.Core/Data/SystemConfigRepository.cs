﻿using Imposto.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Imposto.Core.Data
{
    public class SystemConfigRepository : Conexao, IRepository<SystemConfig>
    {
        public SystemConfig Consultar(int id = 0)
        {
            SqlParameter[] parametros = new SqlParameter[]
           {
                new SqlParameter("pId", id),
           };

            DataSet sysConfig = base.Consultar("dbo.P_CONSULTA_CONFIG", parametros);
            return Converter(sysConfig);
        }

        public void Excluir(int id)
        {
            SqlParameter[] parametros = new SqlParameter[]
            {
                new SqlParameter("pId", id)
            };

            base.Executar("dbo.P_DELETAR_CONFIG", parametros);
        }

        public int Salvar(SystemConfig ent)
        {
            foreach (var item in ent.Configs)
            {
                SqlParameter[] parametros = new SqlParameter[]
               {
                    new SqlParameter("pId", item.Id),
                    new SqlParameter("pCampo", item.Campo),
                    new SqlParameter("pValor", item.Valor),
               };

                base.Executar("dbo.P_INCLUIR_ATUALIZAR_CONFIG", parametros);
            }

            return 0;
        }

        List<SystemConfig> IRepository<SystemConfig>.Consultar(int id)
        {
            throw new NotImplementedException();
        }

        SystemConfig IRepository<SystemConfig>.ConsultarPorId(int id)
        {
            throw new NotImplementedException();
        }

        private SystemConfig Converter(DataSet dsConfig)
        {
            if (dsConfig == null && dsConfig.Tables == null && dsConfig.Tables.Count == 0 && dsConfig.Tables[0].Rows.Count == 0)
            {
                throw new Exception("Data Set Notas não é valido");
            }

            SystemConfig systemConfig = new SystemConfig();
            foreach (DataRow row in dsConfig.Tables[0].Rows)
            {
                Config config = new Config()
                {
                    Id = row.Field<int>("Id"),
                    Campo = row.Field<string>("Campo"),
                    Valor = row.Field<string>("Valor"),
                };

                systemConfig.Configs.Add(config);
            }

            return systemConfig;
        }
    }
}
