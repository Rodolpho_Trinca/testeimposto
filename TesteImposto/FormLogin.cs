﻿using Imposto.Core.Domain;
using Imposto.Core.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteImposto
{
    public partial class FormLogin : Form
    {
        public event EventHandler<LoginEventArgs> OnLogin;

        public FormLogin()
        {
            InitializeComponent();
        }

        private void BtnLogar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtLogin.Text))
                    MessageBox.Show("Por favor, digite o usuario");

                if (string.IsNullOrEmpty(txtSenha.Text))
                    MessageBox.Show("Por favor, digite a senha");

                LoginService service = new LoginService();
                var usuario = new Usuario()
                {
                    User = txtLogin.Text,
                };

                usuario.ConverterSenhaHash(txtSenha.Text);
                OnLogin.Invoke(this, new LoginEventArgs(service.LogarUsuario(usuario)));
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erro: {ex.Message}");
            }
        }

        
    }
}
