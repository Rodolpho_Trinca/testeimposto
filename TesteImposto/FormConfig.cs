﻿using Imposto.Core.Domain;
using Imposto.Core.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TesteImposto
{
    public partial class FormConfig : Form
    {
        public FormConfig()
        {
            InitializeComponent();
            ListaConfigs();
        }

        private void BtnSelecionarPasta_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog folderBrowser = new OpenFileDialog())
            {
                folderBrowser.InitialDirectory = string.IsNullOrEmpty(txtNfeCaminhoSaida.Text) ? Environment.CurrentDirectory : txtNfeCaminhoSaida.Text;
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                folderBrowser.FileName = "Selecione a pasta";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    txtNfeCaminhoSaida.Text = Path.GetDirectoryName(folderBrowser.FileName);
                }
            }
        }

        private void ListaConfigs()
        {
            ConfigService service = new ConfigService();

            SystemConfig config = service.Consultar();

            var campoCaminho = "CaminhoSalvarNFE";
            if (config.ExisteCampo(campoCaminho))
            {
                var caminhoSalvarNFE = config.BuscaConfig(campoCaminho);
                txtNfeCaminhoSaida.Text = caminhoSalvarNFE.Valor;
            }
        }

        private void BtnResetar_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Você tem certeza que deseja resetar configurações?", "Resetar configurações", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (dr != DialogResult.Yes)
                return;

            txtNfeCaminhoSaida.Text = Environment.CurrentDirectory;
            Salvar();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSalvar_Click(object sender, EventArgs e)
        {
            Salvar();
        }

        private void Salvar()
        {
            ConfigService service = new ConfigService();

            SystemConfig config = service.Consultar();

            var caminhoSalvarNFE = config.BuscaConfig("CaminhoSalvarNFE");
            caminhoSalvarNFE.Valor = txtNfeCaminhoSaida.Text;

            service.Salvar(config);

            MessageBox.Show("Configurações salvas com sucesso");
        }
    }
}
