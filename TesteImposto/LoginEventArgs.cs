﻿using System;

namespace TesteImposto
{
    public class LoginEventArgs : EventArgs
    {
        public bool UsuarioLogado { get; }

        public LoginEventArgs(bool usuarioLogado)
        {
            this.UsuarioLogado = usuarioLogado;
        }
    }
}