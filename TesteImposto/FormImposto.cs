﻿using Imposto.Core.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Imposto.Core.Domain;

namespace TesteImposto
{
    public partial class FormImposto : Form
    {
        private Pedido pedido = new Pedido();

        public FormImposto()
        {
            InitializeComponent();
            GerarDataGrid();
            AtualizaEstados();
        }

        private object GetTablePedidos()
        {
            DataTable table = new DataTable("pedidos");
            table.Columns.Add(new DataColumn("Nome do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Codigo do produto", typeof(string)));
            table.Columns.Add(new DataColumn("Valor", typeof(decimal)));
            table.Columns.Add(new DataColumn("Brinde", typeof(bool)));
            
            return table;
        }

        private void buttonGerarNotaFiscal_Click(object sender, EventArgs e)
        {
            try
            {
                NotaFiscalService service = new NotaFiscalService();
                pedido.EstadoOrigem = (Estado)cbbEstadoOrigem.SelectedItem;
                pedido.EstadoDestino = (Estado)cbbEstadoDestino.SelectedItem;
                pedido.NomeCliente = txtNomeCliente.Text;

                DataTable table = (DataTable)dataGridViewPedidos.DataSource;

                foreach (DataRow row in table.Rows)
                {
                    pedido.ItensDoPedido.Add(
                        new PedidoItem()
                        {
                            Brinde = row["Brinde"].ToString() == "true" ? true : false,
                            CodigoProduto = row["Codigo do produto"].ToString(),
                            NomeProduto = row["Nome do produto"].ToString(),
                            ValorItemPedido = Convert.ToDecimal(row["Valor"].ToString())
                        });

                }

                service.GerarNotaFiscal(pedido);
                LimparCampos();
                MessageBox.Show("Operação efetuada com sucesso");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Erro: {ex.Message}");
            }
        }

        private void LimparCampos()
        {
            cbbEstadoDestino.SelectedItem = null;
            cbbEstadoOrigem.Text = null;
            txtNomeCliente.Text = "";
            GerarDataGrid();
        }

        private void AtualizaEstados()
        {
            EstadoService service = new EstadoService();

            cbbEstadoDestino.Items.AddRange(service.ListarEstadosDestino().ToArray());
            cbbEstadoOrigem.Items.AddRange(service.ListarEstadosOrigem().ToArray()); 
        }

        private void GerarDataGrid()
        {
            dataGridViewPedidos.AutoGenerateColumns = true;
            dataGridViewPedidos.DataSource = GetTablePedidos();
            ResizeColumns();
        }
        private void ResizeColumns()
        {
            double mediaWidth = dataGridViewPedidos.Width / dataGridViewPedidos.Columns.GetColumnCount(DataGridViewElementStates.Visible);

            for (int i = dataGridViewPedidos.Columns.Count - 1; i >= 0; i--)
            {
                var coluna = dataGridViewPedidos.Columns[i];
                coluna.Width = Convert.ToInt32(mediaWidth);
            }
        }

        private void BtnConfig_Click(object sender, EventArgs e)
        {
            FormLogin formLogin = new FormLogin();
            formLogin.OnLogin += (o, eve) =>
            {
                if (!eve.UsuarioLogado)
                    return;

                Form formConfig = new FormConfig();
                formConfig.Show();
            };
            formLogin.Show();
        }
    }
}
